//
//  Target_UnityAds.h
//  AtmobLib
//
//  Created by CantChat on 26/03/2018.
//

#import <Foundation/Foundation.h>

@interface Target_UnityAds : NSObject

- (BOOL)Action_test:(id)param;
- (BOOL)Action_initialize:(NSDictionary *)params;

- (void)Action_load:(NSDictionary *)params;
- (void)Action_show:(NSDictionary *)params;

@end
