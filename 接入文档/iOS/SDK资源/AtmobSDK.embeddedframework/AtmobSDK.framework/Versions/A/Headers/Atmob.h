//
//  Atmob.h
//  Atmob
//
//  Created by CantChat on 27/01/2018.
//  Copyright © 2018 CantChat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef Atmob_h
#define Atmob_h

typedef NS_ENUM(NSInteger, AtmobAdType) {
    AtmobAdTypeBanner = 10000,  // banner
    AtmobAdTypeInterstitial,    // 插屏
    AtmobAdTypeNative,          // 原生
    AtmobAdTypeRewardVideo,     // 激励视频
    AtmobAdTypeSplash           // 启屏
};

typedef NS_ENUM(NSInteger, AtmobAdError) {
    AtmobAdErrorInitError = 0,  // 初始化还未成功,请联系管理员
    AtmobAdErrorType,           // 广告平台号错误,请联系管理员
    AtmobAdErrorConfig,         // 广告平台还未配置任何默认广告平台,请联系管理员
    AtmobAdErrorNotLoad,        // 还未加载广告
    AtmobAdErrorLoad,           // 加载广告平台广告失败
    AtmobAdErrorLoadNoAd,       // 无广告
    AtmobAdErrorTimeout,        // 加载超时
    AtmobAdErrorEmpty,          // 暂无广告，已被过滤
    AtmobAdErrorShowError       // 展示异常
};

@protocol AtmobAdDelegate <NSObject>
@optional
- (void)atmobLoaded:(NSString* _Nonnull)atmobPid;
- (void)atmobShowSuccess:(NSString* _Nonnull)atmobPid;
- (void)atmobDidError:(AtmobAdError)error WithMessage:(NSString* _Nonnull)message atmobPid:(NSString*_Nonnull)atmobPid;
- (void)atmobAdClicked:(NSString* _Nonnull)atmobPid;
- (void)atmobAdClosed:(NSString* _Nonnull)atmobPid;

- (void)atmobAdRewardSuccess:(NSString* _Nonnull)atmobPid;
- (void)atmobPresentAdShow:(NSString* _Nonnull)atmobPid;
- (void)atmobPresentAdClosed:(NSString* _Nonnull)atmobPid;

@end

typedef void (^AtmobSDKInitCallBack)(BOOL success);

@interface Atmob : NSObject

@property (nonatomic, copy) NSString* _Nonnull appid;

+ (NSString* _Nonnull)SDKVersion;
+ (Atmob* _Nonnull)sharedInstance;

- (void)initialize:(NSString* _Nonnull)appid;
- (void)initialize:(NSString* _Nonnull)appid callBack:(AtmobSDKInitCallBack)callBack;



@end

#endif
