//
//  AtmobBannerAd.h
//  AtmobSDK
//
//  Created by CantChat on 14/03/2018.
//

#import "AtmobAd.h"

typedef NS_ENUM(NSInteger, AtmobBannerPosition) {
    AtmobBannerPositionTop, // 默认顶部
    AtmobBannerPositionBottom
};

@interface AtmobBannerAd : AtmobAd

@property (nonatomic, assign) AtmobBannerPosition position;

@end
