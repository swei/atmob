# Atmob 广告聚合SDK(iOS)接入文档
### 1.更新历史
#### 2018-06-21 v0.1.3
```
- 新增激励视频激励成功回调
- 修复广点通原生banner和Unity激励视频重复加载问题
- 替换Unity framework, 修复展示后重新load必然失败的问题
- 修复Unity激励视频重新加载时无法正确获取回调的问题
```
#### 2018-06-07 v0.1.2
```
- 隐藏部分变量
- 修复广点通源生的位置问题
```
#### 2018-05-08 v0.1.1
```
- 启用了TalkingData追踪数据
```
#### 2018-04-28 v0.1.1
```
- 增加AdHub广告平台支持
- 修复了一些可能导致crash的问题
```

#### 2018-04-20 v0.1.0
```
- 内测版本
```

### 2.前言
此接入文档适用于所有接入atmob聚合广告平台产品，请开发者在后台提交相关产品信息，提交后通知我方工作人员，审核通过后即可获取appid，之后即可添加您需要的广告位，获取相应广告位pid，接入过程中需要这两个参数。

**appid、pid、包名是相互绑定的，请切记！**

### 3.准备工作
请参考实例工程引入的libraries，列举如下：
```
- AdSupport.framework
- AVFoundation.framework
- CFNetwork,framework
- CoreGraphics.framework
- CoreLocation.framework
- CoreMedia.framework
- CoreMotion.framework
- CoreTelephony.framework
- JavaScriptCore.framework
- MessageUI.framework
- MobileCoreServices.framework
- QuartzCore.framework
- SafariServices.framework
- Security.framework
- StoreKit.framework
- SystemConfiguration.framework
- Twitter.framework
- WebKit.framework
- libz.tbd
- libc++.tbd
- libxml2.tbd
- libsqlite3.tbd
```

接下来，将AtmobSDK的静态库引入工程
- 主工程`AtmobSDK.embeddedframework`文件夹
- 其他AtmobSDK开头的广告平台相关frameworks
- libTalkingDataGA.a
将工程target下的`Build Settings-Other Linker Flags`增加一条`-ObjC`

### 4.代码接入
#### 1.初始化SDK
在工程AppDelegate.m中:
- `#import <AtmobSDK/Atmob.h>`
- `application:didFinishLaunchingWithOptions:`方法中调用以下初始化方法：
```
[[Atmob sharedInstance] initialize:kDemoAppid callBack:^(BOOL success) {
        if (success) {
            // AtmobSDK初始化成功
        }
    }];
```

#### 2.Banner广告接入
在ViewController中的接入示例:
```
// 引入头文件和delegate
#import <AtmobSDK/AtmobBannerAd.h>
@interface ViewController () <AtmobAdDelegate>

// Banner位置, 可选
NSInteger isBottom;

// 声明Banner
@property (nonatomic, strong) AtmobBannerAd *banner;

// 在适当的时机创建Banner对象
self.banner = [[AtmobBannerAd alloc] initWithAppid:appid atmobPid:bannerPid viewController:self delegate:self];
    self.banner.position = isBottom;
    [self.banner load];

// 移除banner对象
- (void)dealloc {
    self.banner.delegate = nil;
    self.banner = nil;
}
```

#### 3.启屏广告接入
省去无关代码后,接入示例如下,在AppDelegate.m中:
```
// 引入头文件和delegate
#import <AtmobSDK/AtmobSplashAd.h>
@interface AppDelegate () <AtmobAdDelegate> 

// 声明启屏广告对象
@property (nonatomic, strong) AtmobSplashAd *splash;

// 创建启屏广告对象, 加载广告
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 初始化AtmobSDK成功后
    __weak typeof(self) weakSelf = self;
    [[Atmob sharedInstance] initialize:appid callBack:^(BOOL success) {
        if (success) {
            weakSelf.splash = [[AtmobSplashAd alloc] initWithAppid:appid atmobPid:splashPid viewController:weakSelf delegate:weakSelf];
            [weakSelf.splash load];
        }
    }];
    return YES;
}
// 展示启屏广告
if (self.splash.isReady) {
    [self.splash show];
}

// 移除splash对象
- (void)dealloc {
    self.splash.delegate = nil;
    self.splash = nil;
}
```

#### 4.插屏广告接入
在ViewController中的接入示例:
```
// 引入头文件和delegate
#import <AtmobSDK/AtmobInterstitialAd.h>
@interface ViewController () <AtmobAdDelegate>

// 声明插屏广告对象
@property (nonatomic, strong) AtmobInterstitialAd *interstitial;

// 创建插屏广告对象, 加载广告
self.interstitial = [[AtmobInterstitialAd alloc] initWithAppid:appid atmobPid:interstitialPid viewController:self delegate:self];
[self.interstitial load];

// 显示插屏广告
if (self.interstitial.isReady) {
    [self.interstitial show];
}

// 移除interstitial对象
- (void)dealloc {
    self.interstitial.delegate = nil;
    self.interstitial = nil;
}
```

#### 5.激励视频广告接入
在ViewController中的接入示例:
```
// 引入头文件和delegate
#import <AtmobSDK/AtmobRewardVideoAd.h>
@interface ViewController () <AtmobAdDelegate>

// 声明激励视频广告对象
@property (nonatomic, strong) AtmobRewardVideoAd *rewardVideo;

// 创建激励视频广告对象,加载广告
self.rewardVideo = [[AtmobRewardVideoAd alloc] initWithAppid:appid atmobPid:rewardVideoPid viewController:self delegate:self];
[self.rewardVideo load];

// 显示激励视频广告
if (self.rewardVideo.isReady) {
    [self.rewardVideo show];
}

// 移除rewardVideo对象
- (void)dealloc {
    self.rewardVideo.delegate = nil;
    self.rewardVideo = nil;
}
```
