//
//  RewardVideoViewController.m
//  AtmobLib_Example
//
//  Created by CantChat on 27/03/2018.
//  Copyright © 2018 fzdruid@gmail.com. All rights reserved.
//

#import "RewardVideoViewController.h"
#import "AppDelegate.h"
#import <AtmobSDK/AtmobRewardVideoAd.h>

@interface RewardVideoViewController () <AtmobAdDelegate> {
    AtmobRewardVideoAd *rewardVideo;
    __weak IBOutlet UILabel *descLabel;
}

@end

@implementation RewardVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    descLabel.text = @"准备拉取广告";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    rewardVideo.delegate = nil;
    rewardVideo = nil;
}

- (IBAction)load:(id)sender {
    if (rewardVideo) {
        rewardVideo.delegate = nil;
        rewardVideo = nil;
    }
    rewardVideo = [[AtmobRewardVideoAd alloc] initWithAppid:kDemoAppid atmobPid:kRewardVideoPid viewController:self delegate:self];
    [rewardVideo load];
}

- (IBAction)show:(id)sender {
    if (!rewardVideo) {
        return;
    }
    [rewardVideo show];
}

#pragma mark - AtmobAdDelegate
- (void)atmobLoaded:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobLoaded--------------InterstitialViewController");
    descLabel.text = @"激励视频广告内容拉取成功！请点击show按钮展示";
}

- (void)atmobDidError:(AtmobAdError)error WithMessage:(NSString* _Nonnull)message atmobPid:(NSString*_Nonnull)atmobPid  {
    NSLog(@"atmobDidError--------------InterstitialViewController, %@", message);
    descLabel.text = [NSString stringWithFormat:@"出错了，%@", message];
}

// show
- (void)atmobShowSuccess:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobShowSuccess--------------RewardVideoViewController");
    descLabel.text = @"激励视频广告内容展示成功！";
}

- (void)atmobAdClosed:(NSString *)atmobPid {
    NSLog(@"atmobAdClosed--------------RewardVideoViewController");
}

// 激励成功,可以获得奖励
- (void)atmobAdRewardSuccess:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobAdRewardSuccess--------------RewardVideoViewController");
    descLabel.text = @"激励视频广告激励成功！";
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
