//
//  SplashViewController.m
//  AtmobLib_Example
//
//  Created by CantChat on 21/03/2018.
//  Copyright © 2018 fzdruid@gmail.com. All rights reserved.
//

#import "SplashViewController.h"
#import "AppDelegate.h"
#import <AtmobSDK/AtmobSplashAd.h>

@interface SplashViewController () <AtmobAdDelegate> {
    AtmobSplashAd *splash;
    __weak IBOutlet UILabel *descLabel;
}

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    descLabel.text = @"准备拉取广告";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    splash.delegate = nil;
    splash = nil;
}

- (IBAction)load:(id)sender {
    if (splash) {
        splash.delegate = nil;
        splash = nil;
    }
    splash = [[AtmobSplashAd alloc] initWithAppid:kDemoAppid atmobPid:kSplashPid viewController:self delegate:self];
    [splash load];
}

- (IBAction)show:(id)sender {
    if (!splash) {
        return;
    }
    [splash show];
}

#pragma mark - AtmobAdDelegate
- (void)atmobLoaded:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobLoaded--------------SplashViewController");
//    [descLabel performSelectorOnMainThread:@selector(setText:) withObject:@"启屏广告内容拉取成功！请点击show按钮展示" waitUntilDone:NO];
    descLabel.text = @"启屏广告内容拉取成功！请点击show按钮展示";
}

- (void)atmobDidError:(AtmobAdError)error WithMessage:(NSString* _Nonnull)message atmobPid:(NSString*_Nonnull)atmobPid  {
    NSLog(@"atmobDidError--------------SplashViewController, %@", message);
//    [descLabel performSelectorOnMainThread:@selector(setText:) withObject:[NSString stringWithFormat:@"出错了，%@", message] waitUntilDone:NO];
    descLabel.text = [NSString stringWithFormat:@"出错了，%@", message];
}

// show
- (void)atmobShowSuccess:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobShowSuccess--------------SplashViewController");
//    [descLabel performSelectorOnMainThread:@selector(setText:) withObject:@"启屏广告展示成功！请返回或重新拉取" waitUntilDone:NO];
    descLabel.text = @"启屏广告展示成功！请返回或重新拉取";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
