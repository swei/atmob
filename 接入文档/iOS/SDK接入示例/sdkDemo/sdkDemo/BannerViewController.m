//
//  BannerViewController.m
//  AtmobLib_Example
//
//  Created by CantChat on 21/03/2018.
//  Copyright © 2018 fzdruid@gmail.com. All rights reserved.
//

#import "BannerViewController.h"
#import "AppDelegate.h"
#import <AtmobSDK/AtmobBannerAd.h>


@interface BannerViewController () <AtmobAdDelegate> {
    AtmobBannerAd *banner;
    NSInteger isBottom;
}

@end

@implementation BannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    banner.delegate = nil;
    banner = nil;
}

- (IBAction)positionChanged:(UISegmentedControl *)sender {
    isBottom = sender.selectedSegmentIndex;
}

- (IBAction)loadBanner:(id)sender {
    if (banner) {
        banner.delegate = nil;
        banner = nil;
    }
    banner = [[AtmobBannerAd alloc] initWithAppid:kDemoAppid atmobPid:kBannerPid viewController:self delegate:self];
    banner.position = isBottom;
    [banner load];
}


#pragma mark - AtmobAdDelegate
- (void)atmobLoaded:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobLoaded--------------BannerViewController");
}

- (void)atmobDidError:(AtmobAdError)error WithMessage:(NSString* _Nonnull)message atmobPid:(NSString*_Nonnull)atmobPid  {
    NSLog(@"atmobDidError--------------BannerViewController, %@", message);
}

- (void)atmobShowSuccess:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobShowSuccess--------------BannerViewController");
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
