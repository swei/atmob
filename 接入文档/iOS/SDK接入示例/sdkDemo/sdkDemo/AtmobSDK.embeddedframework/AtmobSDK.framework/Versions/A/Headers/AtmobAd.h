//
//  AtmobAd.h
//  AtmobSDK
//
//  Created by CantChat on 14/03/2018.
//

#import <Foundation/Foundation.h>


#import "Atmob.h"

@interface AtmobAd : NSObject

@property (nonatomic, copy) NSString *_Nonnull appid;
@property (nonatomic, copy) NSString *_Nonnull atmobPid;
@property (nonatomic, weak) id<AtmobAdDelegate>_Nullable delegate;
@property (nonatomic) BOOL isReady;


@property (nonatomic, copy) NSString * _Nullable currentAdPlatformPid;

- (id _Nullable )initWithAppid:(NSString *_Nonnull)appid
                      atmobPid:(NSString *_Nonnull)atmobPid
                viewController:(UIViewController *_Nullable)viewController
                      delegate:(id <AtmobAdDelegate> _Nullable)delegate;


- (void)load;
- (void)show;

- (AtmobAdType)adType;

@end
