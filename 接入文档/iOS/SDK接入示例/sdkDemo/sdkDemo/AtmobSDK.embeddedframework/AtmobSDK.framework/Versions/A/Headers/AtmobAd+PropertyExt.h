//
//  AtmobAd+PropertyExt.h
//  AtmobSDK
//
//  Created by CantChat on 28/05/2018.
//

#import "AtmobAd.h"

@interface AtmobAd ()

@property (nonatomic, weak) UIViewController *_Nullable viewController;
// 广告位对象实例容器
@property (nonatomic, strong) NSMutableDictionary *_Nonnull adsInstanceDict;

@end
