//
//  Target_GDTMob.h
//  AtmobLib
//
//  Created by CantChat on 08/03/2018.
//

#import <Foundation/Foundation.h>

@interface Target_GDTMob : NSObject

- (BOOL)Action_test:(id)param;
- (BOOL)Action_initialize:(NSDictionary *)params;

- (void)Action_load:(NSDictionary *)params;
- (void)Action_show:(NSDictionary *)params;

- (void)Action_attach:(NSDictionary *)params;
- (void)Action_click:(NSDictionary *)params;

@end
