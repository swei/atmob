//
//  AtmobSDK_GDTMob.h
//  AtmobSDK_GDTMob
//
//  Created by CantChat on 11/04/2018.
//  Copyright © 2018 CantChat. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AtmobSDK_GDTMob.
FOUNDATION_EXPORT double AtmobSDK_GDTMobVersionNumber;

//! Project version string for AtmobSDK_GDTMob.
FOUNDATION_EXPORT const unsigned char AtmobSDK_GDTMobVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AtmobSDK_GDTMob/PublicHeader.h>

#import <AtmobSDK_GDTMob/Target_GDTMob.h>
