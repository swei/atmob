//
//  AppDelegate.m
//  sdkDemo
//
//  Created by CantChat on 19/04/2018.
//  Copyright © 2018 CantChat. All rights reserved.
//

#import "AppDelegate.h"
#import <AtmobSDK/Atmob.h>
#import <AtmobSDK/AtmobSplashAd.h>

@interface AppDelegate () <AtmobAdDelegate> {
    
}

@property (nonatomic, strong) AtmobSplashAd *splash;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    __weak typeof(self) weakSelf = self;
    [[Atmob sharedInstance] initialize:kDemoAppid callBack:^(BOOL success) {
        NSLog(@"初始化结果: %d", success);
        if (success) {
            weakSelf.splash = [[AtmobSplashAd alloc] initWithAppid:kDemoAppid atmobPid:kSplashPid viewController:weakSelf delegate:weakSelf];
            [weakSelf.splash load];
        }
    }];
    NSLog(@"沙盒路径:%@", NSHomeDirectory());
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - AtmobAdDelegate
- (void)atmobLoaded:(NSString* _Nonnull)atmobPid {
    if (self.splash.isReady) {
        [self.splash show];
    }
    
}

- (void)atmobDidError:(AtmobAdError)error WithMessage:(NSString* _Nonnull)message atmobPid:(NSString*_Nonnull)atmobPid  {
    
}

- (void)atmobShowSuccess:(NSString* _Nonnull)atmobPid {
    
}
@end
