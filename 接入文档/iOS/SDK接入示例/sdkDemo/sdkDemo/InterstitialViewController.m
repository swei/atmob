//
//  InterstitialViewController.m
//  AtmobLib_Example
//
//  Created by CantChat on 21/03/2018.
//  Copyright © 2018 fzdruid@gmail.com. All rights reserved.
//

#import "InterstitialViewController.h"
#import "AppDelegate.h"
#import <AtmobSDK/AtmobInterstitialAd.h>

@interface InterstitialViewController () <AtmobAdDelegate> {
    AtmobInterstitialAd *interstitial;
    __weak IBOutlet UILabel *descLabel;
}

@end

@implementation InterstitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    descLabel.text = @"准备拉取广告";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    interstitial.delegate = nil;
    interstitial = nil;
}

- (IBAction)load:(id)sender {
    if (interstitial) {
        interstitial.delegate = nil;
        interstitial = nil;
    }
    interstitial = [[AtmobInterstitialAd alloc] initWithAppid:kDemoAppid atmobPid:kInterstitialPid viewController:self delegate:self];
    [interstitial load];
    descLabel.text = @"正在拉取插屏广告内容";
}

- (IBAction)show:(id)sender {
    if (!interstitial) {
        return;
    }
    [interstitial show];
}

#pragma mark - AtmobAdDelegate
- (void)atmobLoaded:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobLoaded--------------InterstitialViewController");
//    [descLabel performSelectorOnMainThread:@selector(setText:) withObject:@"插屏广告内容拉取成功！请点击show按钮展示" waitUntilDone:NO];
    descLabel.text = @"插屏广告内容拉取成功！请点击show按钮展示";
}

- (void)atmobDidError:(AtmobAdError)error WithMessage:(NSString* _Nonnull)message atmobPid:(NSString*_Nonnull)atmobPid  {
    NSLog(@"atmobDidError--------------InterstitialViewController, %@", message);
//    [descLabel performSelectorOnMainThread:@selector(setText:) withObject:[NSString stringWithFormat:@"出错了，%@", message] waitUntilDone:NO];
    descLabel.text = [NSString stringWithFormat:@"出错了，%@", message];
}

// show
- (void)atmobShowSuccess:(NSString* _Nonnull)atmobPid {
    NSLog(@"atmobShowSuccess--------------InterstitialViewController");
//    [descLabel performSelectorOnMainThread:@selector(setText:) withObject:@"插屏广告内容展示成功！" waitUntilDone:NO];
    descLabel.text = @"插屏广告内容展示成功！";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
