# Atmob 广告聚合SDK(安卓)接入文档 
### 1.更新历史
#### 2018-04-27 v0.11
```
1，修复当应用targetsdk>22百度广告应用安装问题
2，调整缓存加载策略，用于处理服务端紧急情况
3，调整加载失败回调文案，依赖于服务端文案提示
4，调整启屏广告图加载，会根据横竖屏以及图片资源尺寸进行调整，加载时不再变形
5，一些特殊情况下的crash

验证时注意清单文件中，有两处是根据应用包名进行变化，常规AndroidStudio接入会自动变更这两处
android:authorities="air.jp.globalgear.kaihi2.xc.fileprovider" 这里是包名.fileprovider
android:authorities="air.jp.globalgear.kaihi2.xc.bd.provider"  这里是包名.bd.fileprovider

以下是示例
<provider
    android:name="android.support.v4.content.FileProvider"
    android:authorities="air.jp.globalgear.kaihi2.xc.fileprovider"
    android:exported="false"
    android:grantUriPermissions="true">
<meta-data
    android:name="android.support.FILE_PROVIDER_PATHS"
    android:resource="@xml/gdt_file_path" />
</provider>


<provider
    android:name="com.baidu.mobads.openad.FileProvider"
    android:authorities="air.jp.globalgear.kaihi2.xc.bd.provider"
    android:exported="false"
    android:grantUriPermissions="true">
    <meta-data
        android:name="android.support.FILE_PROVIDER_PATHS"
        android:resource="@xml/bd_file_paths" />
</provider>
```

#### 2018-04-11 v0.10
```
1，裁剪资源文件
2，优化上报
```

#### 2018-04-05 v0.01
```
1，内测版本
```

##### 注意：此版本为内测版本，聚合四家广告平台，四家通过测试，初次接入至少添加一家以上广告平台，资源为atmob_sdk_开头.aar，更新时代码不变，更换资源即可;


### 2.前言
此接入文档适用于所有接入atmob聚合广告平台产品，请开发者在后台提交相关产品信息，提交后通知我方工作人员，审核通过后即可获取appid，之后即可添加您需要的广告位，获取相应广告位pid，接入过程中需要这两个参数。

**appid、pid、包名是相互绑定的，请切记！**

### 3.准备工作
请参考示例工程目录结构，以及build.grade内容
```
加入此节点  
    repositories {
        flatDir {
            dirs 'libs'
        }
    }
引入libs目录的aar文件：
dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    compile(name: 'atmob_imgloader-debug', ext: 'aar')
    compile(name: 'atmob_library-debug', ext: 'aar')
    compile(name: 'atmob_plugin-debug', ext: 'aar')
    compile(name: 'atmob_sdk-debug', ext: 'aar')
    compile(name: 'atmob_sdk_adhub-debug', ext: 'aar')
    compile(name: 'atmob_sdk_baidu-debug', ext: 'aar')
    compile(name: 'atmob_sdk_gdt-native-debug', ext: 'aar')
    compile(name: 'atmob_sdk_gdt-debug', ext: 'aar')
    compile(name: 'adhub-sdk-release', ext: 'aar')
    
    //引入v4,v7包,如果工程原本引用了则去除
    compile(name: 'support-v4-23.2.0', ext: 'aar')
    compile(name: 'support-appcompat-v7-23.2.0', ext: 'aar')
    //或者删除libs目录aar，用在线引用方式，比如:implementation 'com.android.support:appcompat-v7:26.1.0'
}


```

### 4.代码接入
#### 1.在Application入口类加入以下代码：
```
public class SampleApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //添加此行SDK代码
        AtmobSDK.attachBaseContext(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //添加此行SDK代码
        AtmobSDK.onApplicationInit(this);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        //添加此行SDK代码
        AtmobSDK.onTerminate(this);
    }
}
```
##### 注意：如果无Application类，则在清单文件Application节点添加继承Application的入口类，在生命周期函数下添加以下代码


#### 2.在Activity中加入以下代码：

```
public class MainActivity extends Activity {
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AtmobSDK.onCreate(this);
    }
    @Override
    protected void onStart() {
        super.onStart();
        AtmobSDK.onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AtmobSDK.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AtmobSDK.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AtmobSDK.onStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AtmobSDK.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AtmobSDK.onActivityResult(this,requestCode,resultCode,data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        AtmobSDK.onNewIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantResults){
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        AtmobSDK.onRequestPermissionsResult(this,requestCode,permissions,grantResults);
    }
}
```
#### 3.初始化SDK

```
public class SampleApplication extends Application {
    public static boolean adInit = false;

    @Override
    public void onCreate() {
        super.onCreate();
        //带回调的
        AtmobSDK.getInstance().initialize(this, "appid", new AtmobSystemListener() {
            @Override
            public void onInitSuccess() {
                adInit = true;
            }

            @Override
            public void onInitFailed(String reason) {
                adInit = false;
            }
        });
        //不带回调的
        AtmobSDK.getInstance().initialize(this, "appid");
    }
}
```
##### 注意：SDK初始化函数应尽早调用，比如在Application onCreate调用，这样有利于对广告模块的加载， 带回调或不带回调选择其一即可 

#### 4.Banner广告接入
##### 1、快速简易集成，依托于Activity 
```
//根据广告位，将banner展现到当前activity的顶部
AtmobEasyBannerAD.showTopBanner(Activity activity, String pid, AtmobADStatusListener listener);

//根据广告位，将banner展现到当前activity的底部
AtmobEasyBannerAD.showBottomBanner(Activity activity, String pid, AtmobADStatusListener listener);

//隐藏当前展现的banner
AtmobEasyBannerAD.hideBanner(String pid);

//移除当前展现的banner，下次调用展示函数，会重新加载广告
AtmobEasyBannerAD.removeBanner(String pid);

//销毁并移除当前banner广告
AtmobEasyBannerAD.destory(String pid);
```
##### 2、自定义横幅广告，依托于ViewGroup

```
 代码方式：
 AtmobBannerView atmobBannerView = new AtmobBannerView(MainActivity.this);
 container = findViewById(R.id.viewgroup_splash);
 container.addView(atmobBannerView);
                
 atmobBannerView.setADListener(new ToastADStatusListener(MainActivity.this));
 atmobBannerView.load(bannerPID);


 xml方式：
 <FrameLayout
        android:id="@+id/viewgroup_splash"
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <com.atmob.ads.sdk.core.AtmobBannerView
            android:layout_width="match_parent"
            android:layout_height="60dp"/>

</FrameLayout>
```

#### 5.启屏广告接入

```
AtmobSplashAD splashAD = new AtmobSplashAD(Activity activity);
splashAD.setADListener(AtmobADStatusListener listener);
//load 数据
splashAD.load(splashPID);

//展示数据
splashAD.show(Activity activity,ViewGroup container);
```

#### 6.插屏广告接入

```
final AtmobInterstitialAD atmobInterstitialAD = new AtmobInterstitialAD(Activity activity);
atmobInterstitialAD.setADListener(AtmobADStatusListener listener);
//load 数据
atmobInterstitialAD.load(interstitialPID);
//展示数据
if(atmobInterstitialAD.isReady){
    atmobInterstitialAD.show(MainActivity.this);
   }
```

#### 7.其它
##### 各广告位回调函数AtmobADStatusListener
```
public interface AtmobADStatusListener {
    void onError(int status , String error);

    void onLoaded();

    void onShow();

    void onClicked();

    void onClose();
}

```


### 5.打包测试

```


```

### 6.日志
在logcat使用"atmob"作为日志过滤标签，可以查询到SDK相关信息。
```
AtmobSDK.setDebuggable(true);
```

### 7.常见问题    
##### 1、咨询409397217
