package air.jp.globalgear.kaihi2.xc;

import android.app.Application;
import android.content.Context;

import com.atmob.ads.library.LibsCore;
import com.atmob.ads.library.network.request.AtmobRequest;
import com.atmob.ads.sdk.core.AtmobSDK;

/**
 * 作者：Swei on 2018/1/29 14:09<BR/>
 * 邮箱：sweilo@qq.com
 */

public class SampleApplication extends Application {
    public static boolean adInit = false;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        AtmobSDK.attachBaseContext(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LibsCore.setHttpType(AtmobRequest.RUN);
        AtmobSDK.onApplicationInit(this);
        //不带回调的
        AtmobSDK.setDebuggable(true);
        AtmobSDK.getInstance().initialize(this, Config.APPID);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        AtmobSDK.onTerminate(this);
    }

}
