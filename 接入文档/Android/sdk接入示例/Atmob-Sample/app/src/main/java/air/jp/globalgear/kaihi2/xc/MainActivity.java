package air.jp.globalgear.kaihi2.xc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.atmob.ads.sdk.core.AtmobEasyBannerAD;
import com.atmob.ads.sdk.core.AtmobInterstitialAD;
import com.atmob.ads.sdk.core.AtmobSDK;
import com.atmob.ads.sdk.core.AtmobSplashAD;
import com.atmob.ads.sdk.listener.AtmobADStatusListener;


public class MainActivity extends Activity {
    private ViewGroup container;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AtmobSDK.onCreate(this);

        setContentView(R.layout.activity_main);
        container = findViewById(R.id.viewgroup_splash);

        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "整合广告平台数:" + AtmobSDK.getAdSDKCode().split("\\|").length, Toast.LENGTH_SHORT).show();
            }
        });
        //easy 方式接入banner广告
        findViewById(R.id.btn_banner).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AtmobEasyBannerAD.showTopBanner(MainActivity.this, Config.BANNERPID, new ToastADStatusListener(MainActivity.this));
            }
        });
        // 隐藏当前banner广告
        findViewById(R.id.btn_hide_banner).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AtmobEasyBannerAD.removeBanner(Config.BANNERPID);
            }
        });
        //展示启屏广告
        findViewById(R.id.btn_splash).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AtmobSplashAD splashAD = new AtmobSplashAD(MainActivity.this);
                splashAD.setADListener(new ToastADStatusListener(MainActivity.this) {

                    @Override
                    public void onLoaded() {
                        super.onLoaded();
                        container.setVisibility(View.VISIBLE);
                        splashAD.show(MainActivity.this, container);
                    }

                    @Override
                    public void onShow() {
                        super.onShow();
                    }

                    @Override
                    public void onClose() {
                        super.onClose();
                        container.setVisibility(View.GONE);
                    }
                });
                splashAD.load(Config.SPLASHPID);
            }
        });

        findViewById(R.id.btn_interstitial).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AtmobInterstitialAD atmobInterstitialAD = new AtmobInterstitialAD(MainActivity.this);
                atmobInterstitialAD.setADListener(new ToastADStatusListener(MainActivity.this) {
                    @Override
                    public void onLoaded() {
                        super.onLoaded();
                        if (atmobInterstitialAD.isReady) {
                            atmobInterstitialAD.show(MainActivity.this);
                        }
                    }
                });
                atmobInterstitialAD.load(Config.INTERSTITIALPID);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        AtmobSDK.onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AtmobSDK.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AtmobSDK.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        AtmobSDK.onStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AtmobSDK.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AtmobSDK.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        AtmobSDK.onNewIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AtmobSDK.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    private static class ToastADStatusListener implements AtmobADStatusListener {
        private Context context;

        public ToastADStatusListener(Context context) {
            this.context = context;

        }

        @Override
        public void onError(int status, String error) {
            Toast.makeText(context, "onError,code:" + status + ",msg:" + error, Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onLoaded() {
            Toast.makeText(context, "onLoaded", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onShow() {
            Toast.makeText(context, "onShow", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onClicked() {
            Toast.makeText(context, "onClicked", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onClose() {
            Toast.makeText(context, "onClose", Toast.LENGTH_SHORT).show();
        }
    }
}
