#### 运营setting服务接口定义 接口说明
#### 正式地址
https://setting.atmob.com

#### 测试地址
https://test.atmob.com

##### 1.加载开发者信息
    GET /setting/dev/loadingDev

##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码 1,2,3,4|否
limit|int|每一页显示条目数|否
dev_account|string|账户名称|否
uid|long|账户id|否
dev_status|int|开发者账户状态 1.正常 2.禁用| 否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
total|int|条目总数用于分页|是
count|int|账户个数|是
dev_list|json array|开发者账户列表|是
uid|long|账户ID|是
dev_account|string|账户名称|是
android_internal_count|int|安卓国内个数|是
android_over_sea_count|int|安卓国外|是
ios_internal_count|int|ios国内|是
ios_over_sea_count|int|ios国外|是
dev_status_des|string|开发者账户状态 1-开通 2-禁用| 是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
        total:100,
        dev_list:[
            {
                uid:1,
                dev_account:"开发者1",
                android_internal_count:10,
                android_over_sea_count:1,
                ios_internal_count:1,
                ios_over_sea_count:1,
                dev_status_des:""
            }
        ]
    }
}
```
##### 2.设置开发者状态
    GET /setting/dev/devStatusChange
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
uid|long|账户id|是
dev_status|开发者账户状态 1.开通 2.禁用| 是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```json
{
    code:0,
    msg:"",
    data:{
    }
}
```

##### 3.加载应用
    GET /setting/app/loadingApp
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码 1,2,3,4|否
limit|int|每一页显示条目数|否
dev_account|string|账户名称|否
app_id|string|应用ID|否
app_name|string|应用名称|否
app_status|int|筛选应用，应用状态 1.开通 2.审核中 3.封禁 4.当前应用以及被删除| 否
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
total|int|条目总数用于分页|是
app_list|json object||否
id|long|当前应用在setting数据仓库的流水ID|是
app_id|string|应用ID|是
app_name|string|应用名称|是
app_os|string|应用类型|是
app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他 |是
ad_space_count|int|广告位个数|是
dev_name|string|开发者|是
app_status_des|string|应用状态 1-正常 2-审核中 3-封禁 4-当前应用以及被删除|是
app_create_time|string|应用创建时间|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
        total:100,
        app_list:[
            {
                id:1,
                app_id:"",
                app_name:"打砖块",
                app_platform_des:"",
                ad_space_count:1,
                app_status_des:"",
                dev_name:"石头哥",
                app_create_time:"2018-10-01 10:00:00"
            }
        ]
    }
}
```
##### 4.设置应用状态
    GET /setting/app/appStatusChange
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用ID|是
app_status|开发者账户状态 1.正常 2.审核中 3.封禁| 是

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

##### 5.查看应用详情
    GET /setting/app/appDetail
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用ID|是

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空   
app_id|string|应用ID|是
app_name|string|应用名称|是
app_platform_des|string|android ，ios |是
dev_name|string|开发商|是
app_package_name|string|包名|是
app_status_des|string|应用合作状态 1-正常 2-审核中 3-封禁 4-当前应用以及被删除|是
app_type_des|string|应用分类 |是
third_platform_list|json object|三方平台数组|是
id|long|三方广告平台流水ID|是
platform_name|string|三方广告平台名称|是
platform_app_params|string|三方广告平台app_id维度参数|是
platform_status_des|string|平台状态 1-正常 2-封禁|是
platform_default|bool|是否默认平台|是
ad_space_list|json object|广告位数组|是
ad_space_name|string|广告位名称|是
atmob_pid|string|atmob平台广告位pid|是
ad_space_id|long|当前广告位的流水ID|是
pid_count|int|已配置广告位个数|是
ad_space_type_des|int|广告位类型 1-启屏 2-插屏 3-banner 4-激励视频 5-原生广告|是
route_group_id|string|策略组id|是
route_group_name|string|策略组名称|是
note|string|备注|是

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
      app_id:"",
      app_name:"",
      app_status_des:"",
      app_platform_des:"",
      dev_name:"",
      app_package_name:"",
      app_type_des:"",
      third_platform_list:[
       {  
                id:"",
                platform_name:"",
                platform_app_params:"",
                platform_status_des:1
       }
      ],
      ad_space_list:[ 
        {
                ad_space_id:1,
                ad_space_name:"",
                atmob_pid:"",
                pid_count:1,
                ad_space_type_des:"",
                route_group_id:"",
                route_group_name:""
        }
      ],
      note:""
    }
}
```
##### 6.修改合作状态
* 此接口走4接口，设置应用状态。

##### 7.修改应用备注
    GET /setting/app/changeNote
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用ID|是
note|string|修改备注|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 8.应用添加三方平台时，加载三方平台列表
    GET /setting/app/loadingThirdPlatform
##### 提交参数说明
参数字段 | 类型|说明
------------| -------------|-------------
appPlatformID|int|应用平台编号|是


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
platform_list|json object|三方平台数组|是
platform_id|string|三方平台对应的id|是
platform_name|string|三方平台对应的名称|是
app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他 |是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
      platform_list:[
        {
          platform_id:"",
          platform_name:"",
          app_platform_des:""
        }
      ]
    }
}
```

##### 9.应用添加三方平台appid信息
    POST /setting/app/addThirdPlatform
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|atmob平台对应的应用ID|是
platform_id|string|三方平台对应的id|是
platform_name|string|三方平台对应的名称|是
platform_app_params|string|三方广告平台app_id维度参数|是
platform_status|int| 平台状态 1.正常 2.禁用|是
app_platform|int|选择地域|是

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 10.查看广告位详情
    GET /setting/app/adSpace/adSpaceDetail
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|平台对应的应用ID|是
ad_space_id|long|当前广告位的流水ID|是

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
app_id|string|应用appid|是
ad_space_id|long|当前广告位的流水ID|是
atmob_pid|string|atmob平台广告位pid|是
ad_space_name|string|广告位名称|是
ad_space_status_des|string|广告位状态 1-正常 2-审核中 3-封禁；4-当前记录已删除；|是
platform_info_list|json object|平台信息列表|是
id|long|三方平台对应pid的流水ID|是
platform_id|string|三方平台对应的id|是
platform_name|string|三方平台对应的名称|是
platform_pid|string|三方平台对应的pid|是
platform_pid_status|string|三方平台广告位状态|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
      app_id:"",
      ad_space_id:1,
      atmob_pid:"",
      ad_space_name:"",
      ad_space_status_des:"",
      platform_info_list:[
        {
          id:1,
          platform_id:"",
          platform_name:"",
          platform_pid:"",
          platform_pid_status:""
        }
      ]
    }
}
```
##### 11.设置广告位状态
    GET /setting/app/adSpaceStatusChange
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用appid|是
ad_space_id|long|当前广告位的流水ID|是
ad_space_status|int|广告位状态 1-正常 2-审核中 3-封禁；4-当前记录已删除；|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

##### 12.加载策略组列表
    GET /setting/app/loadingRouteGroups
##### 提交参数说明

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
route_group_id|string|策略组id|是
route_group_name|string|策略组名称|是
route_group_list|json object|策略组|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
      route_group_list:[
        {
          route_group_id:"",
          route_group_name:""
        }
      ]
    }
}
```
##### 13.修改广告位策略组
    GET /setting/app/changeRouteGroup
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用appid|是
ad_space_id|long|当前广告位的流水ID|是
route_group_id|string|策略组id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

##### 14.加载广告位信息
    GET /setting/adSpace/loading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码 1,2,3,4|否
limit|int|每一页显示条目数|否
app_name|string|应用名称|否
dev_account|string|开发者|否
app_id|string|应用appid|否
ad_space_status|int|广告位状态 1.正常 2.审核中 3.封禁；4.当前记录已删除； |否
app_platform|int|应用平台 1.安卓国内 2.google商店 3.ios应用商店 4.其他|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
app_count|int|应用个数|是
ad_space_list|json object|广告位详情列表|是
id|long|广告位流水ID|是
app_id|string|应用appid|是
app_name|string|应用名称|是
ad_space_name|string|广告位名称|是
atmob_pid|string|atmob平台广告位pid|是
app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他|是
dev_account|string|开发者|是
pid_count|int|已经配置的广告位个数|是
platform_count|int|已经配置的三方广告平台个数|是
ad_space_status_des|string|广告位状态|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
     app_count:1,
     ad_space_list:[
      {
        id:1,
        app_id:"应用appid",
        app_name:"应用名称",
        ad_space_name:"广告位",
        atmob_pid:"广告位id",
        app_platform_des:"", 
        dev_account:"",
        pid_count:3, 
        platform_count:5, 
        ad_space_status_des:""
      }
     ]
    }
}
```

##### 15.广告位配置加载

##### 提交参数说明

 GET /setting/adSpace/pidConfigLoading
 
 * 走接口 10 

参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用id|是
atmob_pid|string|atmob平台广告位聚合id|是

* 修改广告位状态接口11， GET /setting/app/adSpace/adSpaceStatusChange；

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
ad_space_name|string|广告位名称|是
ad_space_status_des|string|广告位状态 1-正常 2-审核中 3-封禁；4-当前记录已删除；|是
atmob_pid|string|atmob平台广告位pid|是
atmob_pid_platform_list|json object|三方平台信息列表|是
platform_id|string|三方平台对应的id|是
platform_name|string|三方平台对应的名称|是
platform_pid|string|三方平台对应广告位的pid结构体|是
platform_pid_status|string|三方平台广告位状态，此字段保留|否

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
      "ad_space_name":"",
      "ad_space_status_des":"",
      "atmob_pid":"",
      "atmob_pid_platform_list":[
        {
          "platform_id":"",
          "platform_name":"",
          "platform_pid":"",
          "platform_pid_status":""
        }
      ]
    }
}
```


##### 16.配置广告位
    POST /setting/adSpace/pidConfig
    Content-type applcaition/json
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_id|string|应用id|是
atmob_pid|string|atmob平台广告位聚合id|是
atmob_pid_platform_list|json object|三方平台信息列表|是
platform_id|string|三方平台对应的id|是
platform_name|string|三方平台对应的名称|是
platform_pid|string|三方平台对应广告位的pid结构体|是
platform_pid_status|int|三方平台广告位状态，此字段保留|否

##### 上行示例
```json
{
         "app_id":"",
         "atmob_pid":"",
         "atmob_pid_platform_list":[
           {
             "platform_id":"",
             "platform_name":"",
             "platform_pid":"",
             "platform_pid_status":"",
           }
         ]
}
```
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
```json
{
    "code":0,
    "msg":"",
    "data":{
    }
}
```



##### 17.策略组加载
    GET /setting/routeGroup/loading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码 1,2,3,4|否
limit|int|每一页显示条目数|否
route_group_name|string|策略组名|否
route_group_id|string|策略组id|否
update_role_name|string|操作人|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
route_group_count|int|策略组个数|是
total|long|策略组总条目|是
route_group_list|json object|是
route_group_id|string|策略组id|是
route_group_name|string|策略组名|是
route_group_status|int|策略组状态 如果是 -2表示异常 |是
app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他 |是
route_count|int|策略个数|是
update_time|string|更新时间|是
update_role_name|string|最后操作|是
id|long|策略组流水id|是

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
     route_group_count:1,
     total:100,
     route_group_list:[
      {
        id:1,
        route_group_id:"策略组id",
        route_group_name:"策略组名",
        route_group_status:1,
        app_platform:1, 
        route_count:3, 
        update_time:"", 
        update_role_name:""
      }
     ]
    }
}
```
##### 18.删除策略组
    GET /setting/routeGroup/del
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_group_id|String|策略组id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 19.查看策略组详情
    GET /setting/routeGroup/detail
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_group_id|string|策略组id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
route_group_name|string|策略组名|是
route_list|json object|策略组列表|是
route_id|string|策略id|是
route_name|string|策略名称|是
route_status|int|策略状态|是
id|long|策略流水ID|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
      route_group_name:"",
      route_list:[
        {
          route_id:"",
          route_name:"",
          id:1
        }
      ]
    }
}
```
##### 20.策略组中删除指定的策略
    GET /setting/routeGroup/delRoute
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_group_id|策略组id|是
route_id|string|策略id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 21.调整策略组中策略顺序|新增策略组
    POST /setting/routeGroup/routeOrder
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_group_id|string|策略组id|否
route_group_name|string|策略组名称|否
update_role_name|string|最后操作人|是
app_platform|int|适用平台|否
route_list|json object|策略组列表|是      
route_id|string|策略id|是              

* 包含策略位置改变，新增策略组

##### 上行示例
```json
{
      "route_group_id":"",
      "route_group_name":"",
      "update_role_name":"",
      "app_platform":1,
      "route_list":[
        {
          "route_id":"",
        }
      ]
}
```

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```


##### 22.加载策略
    GET /setting/route/loading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码 1,2,3,4|是
limit|int|每一页显示条目数|是
route_name|string|策略名称|否
route_id|string|策略Id|否
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
route_count|int|策略个数|是
total|long|策略总条目|是
route_list|json object|策略json数组|是
id|long|策略流水id|是
route_id|string|策略id|是
route_name|string|策略名|是
app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他|是
platform_name|string|平台名|是
update_time|string|更新时间|是
update_role_name|string|最后操作|是
route_status|int|策略状态|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
     route_count:1,
     total:100,
     route_list:[
      {
        id:1,
        route_id:"策略id",
        route_name:"策略名",
        app_platform_des:"", 
        update_time:"", 
        update_role_name:"",
        route_status":1
      }
     ]
    }
}
```
##### 23.删除策略
    GET /setting/route/del
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_id|string|策略id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 24.创建，修改策略
    POST /setting/route/createOrChangeRoute
    content-tyep json
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
id|long|策略流水ID|否
route_id|string|策略名称|否
route_name|string|策略名称|是
app_platform|int|应用平台 1.安卓国内 2.google商店 3.ios应用商店 4.其他|是
update_role_name|string|操作人|是
platform_id|string|平台id|是
platform_name|string|平台名称|是
route_info_detail|json object|策略限制条件详情|否
route_type|int|策略限制类型 1.独立ip展现量; 2.独立ip点击量;3.其他|否
route_conditionals|json object|具体 route_type类型的限制条件|否
route_conditional_times|int|限制次数|否
route_conditional_hour|int|限制小时|否
##### 上行示例
```json
{
    "id":1,
    "route_id":"",
    "route_name": "", 
    "app_platform": 1, 
    "platform_id": "", 
    "platform_name": "", 
    "update_role_name":"",
    "route_info_detail": {
        "show": {
            "route_type": 1, 
            "route_conditionals": [
                {
                    "route_conditional_times": 5, 
                    "route_conditional_hour": 24
                }
            ]
        }, 
        "click": {
            "route_type": 2, 
            "route_conditionals": [
                {
                    "route_conditional_times": 5, 
                    "route_conditional_hour": 24
                }
            ]
        }
    }
}
```

* 策略类型限制，更加route_type定义 对 route_info_detail进行相应节点填充

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

##### 25.查看策略详情
  
   GET  /setting/route/routeDetail
  
##### 提交参数说明

参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_id|string|策略ID|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
id|long|策略流水ID|否
route_id|string|策略名称|否
route_name|string|策略名称|是
app_platform|int|应用平台 1.安卓国内 2.google商店 3.ios应用商店 4.其他|是
update_role_name|string|操作人|是
platform_id|string|平台id|是
platform_name|string|平台名称|是
route_info_detail|json object|策略限制条件详情|否
route_type|int|策略限制类型 1.独立ip展现量; 2.独立ip点击量;3.其他|否
route_conditionals|json object|具体 route_type类型的限制条件|否
route_conditional_times|int|限制次数|否
route_conditional_hour|int|限制小时|否

```json
{
    "code":0,
    "msg":"",
    "data":{
         "id":1,
        "route_id":"",
        "route_name": "", 
        "app_platform": 1, 
        "platform_id": "", 
        "platform_name": "", 
        "update_role_name":"",
        "route_info_detail": {
            "show": {
                "route_type": 1, 
                "route_conditionals": [
                    {
                        "route_conditional_times": 5, 
                        "route_conditional_hour": 24
                    }
                ]
            }, 
            "click": {
                "route_type": 2, 
                "route_conditionals": [
                    {
                        "route_conditional_times": 5, 
                        "route_conditional_hour": 24
                    }
                ]
            }
        }
    }
}
```




##### 26.三方广告平台加载
    GET /setting/platform/loading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码 1,2,3,4|否
limit|int|每一页显示条目数|否
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
total|long|策略总条目|是
platform_list|json object|是
id|long|流水ID|是
platform_id|三方平台ID|是
platform_name|广告平台名称|是
app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他|是
platform_status|int|平台状态|是
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
     total:100,
     platform_list:[
      {
        id:1,
        platform_id:"三方平台ID",
        platform_name:"广告平台名称",
        app_platform_des:""，
        platform_status:1
      }
     ]
    }
}
```
##### 27.三方广告平台删除 
    GET /setting/platform/del
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
platform_id|string|广告平台ID|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 28.三方广告平台添加 
    GET /setting/platform/add
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
app_platform|int|应用平台 1.安卓国内 2.google商店 3.ios应用商店 4.其他|是
platform_name|string|平台名|是
platform_id|string|平台id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

###### 3.16日新增接口 
##### 29.策略状态已删除，恢复策略
    GET /setting/route/reuseRoute
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
route_id|string|策略id|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```
##### 30.删除三方平台
    GET /setting/app/delThirdPlatform
##### 提交参数说明
参数字段 | 类型|说明
------------| -------------|-------------
app_id|string|应用appid|是
platform_id|string|广告平台编号|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

###### 3.20日新增接口 

##### 31.设置默认平台
    GET /setting/app/setPlatformDefault
##### 提交参数说明
参数字段 | 类型|说明
------------| -------------|-------------
app_id|string|应用appid|是
platform_id|string|广告平台编号|是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

