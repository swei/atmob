1.dev首页数据报表接口定义 接口说明

##### 1.获取广告列表基础数据
    GET /dev/data/getData
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
devID|long|开发者ID|是
day|string|时间，不传则为昨日数据，格式为20180410|否


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示正常，其他表示错误，例如用户id传递错误，正式环境下token校验不对
msg        |string |其它异常错误，正确则为空
profit |double|收入数据
show |double|展示次数
click |double|点击次数
ecpm |double|千次展示收益
profitIncrease |string|收入环比百分比，负值表示下降，{昨日-前日}绝对值/前日,昨日-前日是正值则认为是正值，否则为负值
showIncrease |string|展示环比百分比，正值表示上升
clickIncrease |double|点击环比百分比，负值表示下降
ecpmIncrease |double|ecpm环比百分比，负值表示下降

##### 返回示例
```json
{
 "code": 0,
 "msg": "",
 "data": {
    "profit": 1230.0,
    "show": 460200,
    "click": 1300,
    "ecpm": 8.05,
    "profitIncrease": 0.10,
    "showIncrease": -0.5,
    "clickIncrease": -0.1,
    "ecpmIncrease": 0.573
    }
}
```

##### 2.搜索广告位基础数据
GET /dev/data/searchData
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码|是
pageLimit|int|页容量|是
devID|long|开发者ID|是
day|string|搜索时间间隔,例如20180322-20180324，表示22，23，24三天数据，如不传递则为昨天数据，选择框默认为昨日~7天前|否
appPlatform|string|对应平台，android国内，android国外，iOS国内，iOS国外，如不传则代表所有平台|否
appID|string|应用ID|否
adSpaceType|string|广告位类型|否
pid|string|聚合pid|否
location|string|地域，当前版本暂时隐藏选择栏，即为所有地区下的|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示正常，其他表示错误，例如用户id传递错误，正式环境下token校验不对
msg        |string |其它异常错误，正确则为空
total|long|总数据大小|是
day|string|时间
profit |double|收入数据
show |double|展示次数
click |double|点击次数
ecpm |double|千次展示收益

##### 返回示例
```json
{
	"code": 0,
	"msg": "",
	"total":10,
	"data": [{
			"day": 20180310,
			"profit": 1230.0,
			"show": 460200,
			"click": 1300,
			"ecpm": 8.05
		},
		{
			"day": 20180309,
			"profit": 1130.0,
			"show": 360200,
			"click": 1100,
			"ecpm": 10.05
		},
		{
			"day": 20180308,
			"profit": 130.0,
			"show": 30200,
			"click": 100,
			"ecpm": 30.05
		},
		{
			"day": 20180307,
			"profit": 1130.0,
			"show": 310200,
			"click": 1200,
			"ecpm": 10.05
		}
	]

}
```


##### 3.搜索广告位基础数据导出
GET /dev/data/exportData
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
devID|long|开发者ID|是
day|string|搜索时间间隔,例如20180322-20180324，表示22，23，24三天数据，如不传递则为昨天数据，选择框默认为昨日~7天前|否
appPlatform|string|对应平台，android国内，android国外，iOS国内，iOS国外，如不传则代表所有平台|否
appID|string|应用ID|否
adSpaceType|string|广告位类型|否
pid|string|聚合pid|否
location|string|地域，当前版本暂时隐藏选择栏，即为所有地区下的|否

##### 返回参数说明

* contentType： application/octet-stream ，浏览器下载

```









