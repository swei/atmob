### 接口定义

#### 正式地址
https://tracking.atmob.com

#### 测试地址
https://test.atmob.com

#### 请求通用参数
参数字段 | 类型|说明 | 必选
------- |-------------|--------|---
platform        |int       | 平台,1代表Android,2代表iOS     | 是
osVersion       |string       | 系统当前版本                          | 是
packageName     |string       | Android表示包名,iOS对应bundleID     | 是
appVersionName  |string       | 当前应用版本名     | 是
appVersionCode  |string          | 当前应用版本号     | 是
deviceId        |string       | 当前手机唯一号        |是
brand           |string       | 手机厂商                     |否
model           |string       | 手机型号                     |否
gaid            |string       | Google ADID，可能为空         |否
gversion        |string       | Google play 的版本名,国内可能未安装        |否 
idfa            |string       | iOS 广告标识符          |否
idfv            |string       | iOS vendor 标识符       |否
mnc             |string       | 手机运营商代码                |否
mcc             |string       | 手机国家号码               |否
networkType    |int          | 当前网络类型,2G为2,3G为3,4G为4,其它手机网络为5,wifi为10,未知为1   |否
networkMobile  |string       | 运营商网络类型,未知和wifi为空串,其它为api获取对应值              |否
language        |string       | 系统当前语言               |否
timezone        |string       | 标准时间，Locale.ENGLISH   |否
useragent       |string       | 手机访问时ua,如果为空客户端构建方式为下述方式               |是
sdkVersion     |string          | sdk版本号                |是
orientation     |int          | 当前广告页面横竖屏，竖屏是1，横屏是2   | 是
screenSize     |string       | 屏幕大小，构建规则为 宽x高           |是

```
userAgent = "Mozilla/5.0 (Linux; Android " + version + "; " + phoneModel + " Build/" + buildId
					+ ") AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";

userAgent = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";
```

#### 业务接口
#### 1.广告加载
        GET /v1/tracking/adload
#### 接口说明
```
开发者调用具体平台广告加载时，是否能展示对应的状态，时机是load平台广告过程结束，调用该接口
```
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appid           |string    | 广告平台分配给开发者的应用id                  |是
code            |string    | 开发者接入sdk，聚合了对应的广告平台数           |是
platformId      |string    | 三方广告平台id                    |是
adSpaceType     |int       | 具体展示广告位类型                 |是
platformPid     |string    | 服务端分配过来需要加载的pid广告            |是
isdefault       |int       | 1 为默认，为加载分配的广告位，2 pid维度的默认广告位，3 app维度的默认广告位|否
status          |int       | 1 为加载成功，2 为加载失败，3 为无广告
atmobPid        |string   | 聚合pid|是


#### 返回结果说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
无           |无    | 无


#### 返回示例
```json
{
	"code": 0,
	"msg": "",
	"data": {
	 }
}

```
#### 2.广告展示业务状态
        GET /v1/tracking/adshow
#### 接口说明
```
开发者调用平台广告展示时，针对性的同步展示状态
```
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appid           |string    | 广告平台分配给开发者的应用id                  |是
code            |string    | 开发者接入sdk，聚合了对应的广告平台数           |是
platformId      |string    | 三方广告平台id                    |是
adSpaceType     |int       | 具体展示广告位类型                 |是
platformPid     |string    | 服务端分配过来需要加载的pid广告            |是
isdefault       |int       | 1 为默认，为加载分配的广告位，2 pid维度的默认广告位，3 app维度的默认广告位|否
status          |int       | 1 为广告被展示，2 为广告展示失败    |是
atmobPid        |string   | 聚合pid|是

#### 返回结果说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
无           |无    | 无

#### 返回示例
```json
{
	"code": 0,
	"msg": "",
	"data": {
	 }
}

```

#### 3.广告被操作状态
        GET /v1/tracking/adoperate
#### 接口说明
```
开发者调用广告展示后，用户点击、安装或打开应用会调用
```
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appid           |string    | 广告平台分配给开发者的应用id                  |是
code            |string    | 开发者接入sdk，聚合了对应的广告平台数           |是
platformId      |string    | 三方广告平台id                    |是
adSpaceType     |int       | 具体展示广告位类型                 |是
platformPid     |string    | 服务端分配过来需要加载的pid广告            |是
isdefault       |int       | 1 为默认，为加载分配的广告位，2pid维度的默认广告位，2app维度的默认广告位|否
status          |int       | 1 为广告被点击，2 为安装，3 为打开应用，  1、2状态有可能因为广告类型获取不到或不准确  |是
atmobPid        |string   | 聚合pid|是


#### 返回结果说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
无           |无    | 无


#### 返回示例
```json
{
	"code": 0,
	"msg": "",
	"data": {
	 }
}

```




