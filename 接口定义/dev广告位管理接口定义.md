##### 1.广告位加载
    GET /dev/adSpace/adSpaceLoading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码|是
limit|int|页容量|是
devID|long|开发者ID|是
appName|string|应用名称|否
adSpaceName|string|广告位名称|否
appPlatform|string|应用平台|否
pid|string|广告位id|否
adType|string|广告类型|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
total|int|总量
appObjects|json object|是
id|long|广告位流水id|是
adSpaceName|string|广告位名称|是
appID|string|应用ID|是
appName|应用名称|是
pid|string|广告位id|是
adType|string|广告类型|是
appPlatform|string|应用平台|是
adSpaceStatus|string|广告位状态|是

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
     "total":10,
     "appObjects":[
      {
        "id":1,
        "adSpaceName":"三方平台ID",
        "appID":"",
        "appName":"广告平台名称",
        "pid":"",
        "adType":"",
        "appPlatform":"",
        "adSpaceStatus":""
      }
     ]
    }
}
```

##### 2.广告位删除
    GET /dev/adSpace/del
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|是
pid|string|广告位id|是


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
    }
}
```



##### 3.创建广告位
    GET /dev/adSpace/create
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|是
appName|string|应用名称|是
adType|string|广告类型|是
adSpaceName|string|广告位名称|是
appPlatformName|string|应用平台|是


* ps 广告位创建应用加载 接口 ：
GET /dev/adSpace/appLoading

* 广告类型接口：
GET /dev/common/appTypeLoading

* 应用平台接口：

GET /dev/common/appPlatformLoading


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误 1.当前应用处于审核中，无法创建广告位
msg	    |string |其它异常错误，正确则为空
appName|string|应用名称|是
adType|string|广告类型|是
adSpaceName|string|广告位名称|是

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
      "adSpaceName":"",
      "adType":"",
      "appName":"",
    }
}
```


##### 4.加载编辑的广告位
    GET /dev/adSpace/editLoading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|是
pid|string|广告位id|是


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
pid|string|广告位id|否
adSpaceStatus|string|广告位状态|是
adSpaceName|string|广告位名称|是
appName|string|应用名称|是
adType|string|广告类型|是

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
      "pid":"",
      "adSpaceStatus":"",
      "adSpaceName":"",
      "appName":"",
      "adType":""
    }
}
```

##### 5.编辑广告位
    GET /dev/adSpace/edit
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|是
pid|string|广告位id|是
adSpaceName|string|广告位名称|是



##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
      "pid":"",
      "adSpaceStatus":"",
      "adSpaceName":"",
      "appName":"",
      "adType":""
    }
}
```


##### 6.应用加载
   GET /dev/adSpace/appLoading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
devID|long|开发者ID|是
appPlatformName|string|android国内，android国外，iOS国内，iOS国外|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
appObjects|json object| 应用列表|是
appID|string|应用ID|是
appName|string|应用名称|是
appStatus|int|应用 '审核中' 对应的code为 2|是


##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
     "appObjects":[
        {
            "appID":"",
            "appName":"",
            "appStatus":""
        }
     ]
    }
}
```


##### 7.首页广告位数据加载    
 GET /dev/adSpace/headerAdSpaceLoading
 
 ##### 提交参数说明
 参数字段 | 类型|说明 | 必选
 --------|-------------|--------|---
 devID|long|开发者ID|是
 appID|string|应用名称|否
 adSpaceType|string|广告位类型|否
 
 ##### 返回参数
 参数字段 | 类型|说明
 ------------| -------------|-------------
 code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
 msg	    |string |其它异常错误，正确则为空
 objects|json object|是
 adSpaceName|string|广告位名|是
 pid|string|广告位pid|是
 ```json
 {
     "code":0,
     "msg":"",
     "data":{
      "objects":[
       {
          "pid":"",
         "adSpaceName":"",
       }
      ]
     }
 }
 ```





