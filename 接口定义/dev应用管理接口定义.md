##### 1.应用列表加载
    GET /dev/app/appLoading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
page|int|页码|是
limit|int|页容量|是
devID|long|开发者ID|是
appName|string|应用名称|否
appID|string|应用ID|否
appPlatform|string|应用平台|否
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
total|int|总量
appObjects|json object|是
id|long|应用流水id|是
appID|应用ID|是
appName|应用名称|是
appPlatform|string|应用平台|是
appStatus|string|应用状态|是

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
     "total":10,
     "appObjects":[
      {
        "id":1,
        "appID":"三方平台ID",
        "appName":"广告平台名称",
        "appPlatform":"",
        "appStatus":""
      }
     ]
    }
}
```

##### 2.应用删除
    GET /dev/app/del
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空


##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
    }
}
```

##### 3.加载编辑的应用
    GET /dev/app/editLoading
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|是


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
appID|string|应用ID|是
appName|string|应用名称|是
appStatus|string|应用当前状态|是
appType|string|应用分类|是
appOs|string|应用系统|是
appPackageName|string|应用包名|是
appLink|string|应用市场链接地址|是
appRelease|bool|应用是否发布|是
appPlatform|string|应用平台|是

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
      "appID":"",
      "appName":"",
      "appStatus":"",
      "appType":"",
      "appOs":"",
      "appPackageName":"",
      "appLink":"",
      "appRelease":false,
    }
}
```

##### 4.编辑应用
    GET /dev/app/edit

* 运行中仅能编辑应用名称，审核中可以全部编辑    
    
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appID|string|应用ID|是
appName|string|应用名称|否
appPackageName|string|应用包名|否
appLink|string|应用市场链接地址|否
appRelease|bool|应用是否发布|否

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空


##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
    }
}
```


##### 5.创建应用
    POST /dev/app/create
    
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
devID|long|开发者ID|是
appName|string|应用名称|是
appOs|int|0 安卓 1 ios| 是
appType|string|应用分类|是
appPlatform|string|应用平台|是
appPackageName|string|应用包名，ios也是这个字段|是
appLink|string|应用市场链接地址|是
appRelease|bool|应用是否发布|是


```json
{
  "devID":1,
  "appOs":0,
  "appName":"",
  "appType":"",
  "appPlatform":"",
  "appPackageName":"",
  "appLink":"",
  "appRelease":""
}
```


##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
appName|string|应用名称|是
appType|string|应用分类|是
appPlatform|string|应用平台|是
appPackageName|string|应用包名，ios也是这个字段|是
appLink|string|应用市场链接地址|是
appRelease|bool|应用是否发布|是

##### 返回示例
```json
{
    "code":0,
    "msg":"",
    "data":{
      "appName":"",
      "appPlatform":"",
      "appPackageName":"",
      "appType":"",
      "appLink":"",
      "appRelease":false
    }
}
```





