####web前端在一些下拉列表中存在硬编码，便于复用，修改遍历，后端提供硬编码接口
```text
各个接口返回的结构均一致
obj_list|json array|具体类型json对象|是
number|int|编号|是
value|string|显示的值|是
如 1-安卓国内
number: 1
value:安卓国内
```
##### 1.应用平台类型
 GET /setting/hardCode/appPlatForm
* app_platform_des|string|应用平台 1-安卓国内 2-google商店 3-ios应用商店 4-其他 |是
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
obj_list|json array|具体类型json对象|是
number|int|编号|是
value|string|显示的值|是

```json
{
  "code":0,
  "msg":"",
  "obj_list":[
    {
      "number":1,
      "value":""
    }
  ]
}
```
##### 2.广告位状态
 GET /setting/hardCode/adSpaceStatus
##### 3.平台广告位状态
 GET /setting/hardCode/platformPidStatus
* platform_pid_status_des|string|平台广告位状态 1-正常 2-禁用 3-其他|是
##### 4.应用类型
 GET /setting/hardCode/appType
* app_type_des|string|应用分类 1-游戏 2-儿童 3-教育 4-购物 5-直播 6-视频 7-图片 8-工作 9-美食 10-生活 11-健康 12-运动 |是
##### 5.应用状态
 GET /setting/hardCode/appStatus
* app_status_des|string|应用合作状态 1-正常 2-审核中 3-封禁 4-当前应用以及被删除|是
##### 6.开发者状态
 GET /setting/hardCode/devStatus
* app_status_des|string|应用合作状态 1-正常 2-审核中 3-封禁 4-当前应用以及被删除|是
##### 7.三方平台
 GET /setting/hardCode/platform
 1、广点通=1000
 2、百度联盟=1001
 3、有米=1002
 4、adHub=1003
##### 8. 策略限制类型
  GET /setting/hardCode/routeType
##### 9. 广告位类型
   GET /setting/hardCode/adSpaceType
   
##### 10. 平台状态
   GET /setting/hardCode/platStatus
##### 11. 策略组状态
   GET /setting/hardCode/routGroupStatus
##### 12. 策略状态
   GET /setting/hardCode/routStatus
   
  



 
