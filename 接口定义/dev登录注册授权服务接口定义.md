1.登录-注册-授权服务接口定义 接口说明

##### 1.注册为开发者用户
    POST /dev/auth/register.json
    Content-type:application/json
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
username |string       | 用户名  | 是
email|string |email|是
password|string|密码|是
surePassword|string|确认密码|是
phone|string|手机|否
company|string|公司|否
localLanguage|int|0 中文(zh)  1 英文(en)|是

```
{
                username:"",
                email:"",
                password:"",
                surePassword:""，
                phone:"",
                company:"",
                localLanguage:""
            }
```

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示注册成功，响应的邮件验证链接已经发送的注册的邮箱，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```



##### 2.账号注册，邮箱验证接口 （服务端直接处理）

    GET /dev/auth/email-verification/check
    
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
x|string |服务端用于验证邮箱有效性签名|是
    
**** 开发者注册，验证邮箱，点击链接验证成功之后，显示验证结果页面 ，成功自动跳转到个人应用页面 ****

* 备注 @🍐补充 验证结果显示页面跳转的url： ？  ，此url携带参数 key 0 成功 1 验证失败



##### 3.登入开发者账号

    POST /dev/auth/login.json
    
    Content-type:application/json
    
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
email|string |email|是
password|string|密码|是

```
{
    code:0,
    msg:"",
    data:{
        email:"",
        password:""
    }
}
```

##### 返回参数说明

参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示登录成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
token	    |string |对应服务访问令牌，注意web端更新token，后续任何接口均通过header上传校验，对应错误码详见 返回代码code定义
username|string|用户名
userID|long|用户ID

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
        token:"",
        username:""
        userID:1
    }
}
```

##### 4.开发者账号免用邮箱，密码登录(token登录，通常载入登录页面的或者访问主页的时候)--服务端放行此url

    GET /dev/auth/tokenAuthLogin.json
    
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
token|string|访问服务器令牌（web端可存在本地）|是


##### 返回参数说明

参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示登录成功，其他表示msg节点字段返回错误,跳转登录界面
msg	    |string |其它异常错误，正确则为空
token	    |string |对应服务访问令牌，注意web端更新token，后续任何接口均通过header上传校验，对应错误码详见 返回代码code定义
username|string|用户名
userID|long|用户ID

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
        token:"",
        username:"",
        "userID":1
    }
}
```

##### 5.忘记密码

    GET /dev/auth/forgetPassword
   
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
email|string|重置密码的邮箱|是
localLanguage|int|0 中文(zh)  1 英文(en)|是

##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，响应的邮件验证链接已经发送的注册的邮箱，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
    }
}
```

##### 6.忘记密码，邮箱验证接口（服务端直接处理）

    GET /dev/auth/email-verification/forgetPassword
    
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
x|string |服务端用于验证邮箱有效性签名|是
    
**** 重置密码验证邮箱，点击链接验证成功之后，显示验证结果页面 ，成功则自动跳转到重置密码页面 ****

* 备注 @🍐补充 验证结果显示页面跳转的url： ？  ，此url携带参数 key ：0 成功 1 验证失败 ；x：服务端用于验证邮箱有效性签名


##### 7.重置指定邮箱密码接口

    POST /dev/auth/email/reset/password
    
    content-type application/json
    
##### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
email|string|重置密码的邮箱|是
password|string|密码|是
confirmPassword|重置密码|是
token|string|重置密码令牌|是

```text
{
    email:"",
    password:"",
    confirmPassword:"",
    token:""
}
```


##### 返回参数说明

参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示重置密码成功，跳转到个人应用界面，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
token	    |string |对应服务访问令牌，注意web端更新token，后续任何接口均通过header上传校验，对应错误码详见 返回代码code定义
username|string|用户名
userID|long|用户ID

##### 返回示例
```
{
    code:0,
    msg:"",
    data:{
        token:"",
        username:""
        userID:1
    }
}
```