### 接口定义

#### 正式地址
https://sdk.atmob.com
#### 测试地址
https://test.atmob.com
#### 请求通用参数

### 本地测试环境地址：
http://sdk.atmob.com:7001/

参数字段 | 类型|说明 | 必选
------- |-------------|--------|---
platform        |int       | 平台,1代表Android,2代表iOS     | 是
osVersion       |string       | 系统当前版本                          | 是
packageName     |string       | Android表示包名,iOS对应bundleID     | 是
appVersionName  |string       | 当前应用版本名     | 是
appVersionCode  |int          | 当前应用版本号     | 是
deviceId        |string       | 当前手机唯一号        |是
brand           |string       | 手机厂商                     |否
model           |string       | 手机型号                     |否
gaid            |string       | Google ADID，可能为空         |否
gversion        |string       | Google play 的版本名,国内可能未安装        |否 
idfa		|string       | iOS 广告标识符		|否
idfv		|string       | iOS vendor 标识符	|否
mnc             |string       | 手机运营商代码                |否
mcc             |string       | 手机国家号码               |否
networkType    |int          | 当前网络类型,2G为2,3G为3,4G为4,其它手机网络为5,wifi为10,未知为1   |否
networkMobile  |string       | 运营商网络类型,未知和wifi为空串,其它为api获取对应值              |否
language        |string       | 系统当前语言               |否
timezone        |string       | 标准时间，Locale.ENGLISH   |否
useragent       |string       | 手机访问时ua,如果为空客户端构建方式为下述方式               |是
sdkVersion     |int          | sdk版本号                |是
orientation     |int          | 当前广告页面横竖屏，竖屏是1，横屏是2   | 是
screenSize     |string       | 屏幕大小，构建规则为 宽x高           |是

```
userAgent = "Mozilla/5.0 (Linux; Android " + version + "; " + phoneModel + " Build/" + buildId
					+ ") AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";

userAgent = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";
```

#### 业务接口
#### 1.聚合广告平台初始化,获取广告平台参数及全局系统策略
        GET /v1/setting/adinitialize
 
#### 接口说明
```
开发者调用初始化函数，初始化函数调用该接口，根据结果初始化各广告平台
```
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appid           |string    | 广告平台分配给开发者的应用id                  |是
pform            |string    | 开发者接入sdk，聚合了对应的广告平台数           |是

#### 返回结果说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
adPlatforms           |array    | 具体广告平台app维度(媒体)配置参数                  |是
adDefault             |object   | 当广告位加载失败情况下，加载具体广告平台对象及对应广告类型广告位           |是
adAtmobConfig         |string   | base64 encode 聚合平台其它全局配置项|是

#### 返回示例
```json
{
	"code": 0,
	"msg": "",
	"data": {
		"adPlatforms": [{
				"platformID": "广告平台1ID",
				"platformName": "广告平台1名称",
				"appdata": "$Base64(平台app维度基础配置json)"
			},
			{
				"platformID": "广告平台2ID",
				"platformName": "广告平台2名称",
				"appdata": "$Base64(平台app维度基础配置json)"
			}
		],
		"adDefault": {
			"platformID": "默认加载广告平台ID",
			"platformName": "默认加载广告平台名称",
			"appdata": "$Base64(平台app维度基础配置json)",
			"ads": [{
					"adId": "当前广告平台下广告位",
					"adType": "广告类型"
				},
				{
					"adId": "当前广告平台下广告位",
					"adType": "广告类型"
				}
			]
		},
		 "adAtmobConfig":"$base64Encode"
	}
}
```


adAtmobConfig 节点下配置示例 :

```json
{
	"adPreload": true,
	"adAllInit": true,
	"loadType": 1,
	"loadPlugin": true,
	"channel": "default",
	"bugly": "62766c04a9",
	"talkingData": "17286F0EF5E84D13840208F78CF13996",
	"gdt": {
		"banner": {
			"refresh": 90,
			"showClose": true,
			"downloadConfirm": 1
		},
		"interstitial": {
			"showType": 1
		},
		"splash": {
			"fetchDelay": 3000
		}
	},
	"gdtNative": {
		"banner": {
			"autoHide": true,
			"hideDuration": 8000,
			"clickPercent": 0
		},
		"interstitial": {
			"clickDelayClose": 1000,
			"clickPercent": 0
		},
		"splash": {
			"skipTime": 6000,
			"clickPercent": 0,
			"clickDelayClose": 1000
		}
	},
	"baiduSSP": {
		"theme": 1,
		"banner": {
			"size": 1
		},
		"interstitial": {
			"adVideo": false,
			"width": 600,
			"height": 500
		},
		"splash": {
			"cacheMB": 30
		},
		"https": false
	},
	"adHub": {
		"banner": {
			"needPost": 1
		},
		"interstitial": {
			"userDefine": false
		}
	}
}
```


#### 补充
```
关于pform的定义说明：
例如聚合的广告平台一共有：广点通、百度联盟、有米、adHub，都聚合的情况下，则pform=1000|1001|1002|1003

例如开发者选择了广点通、百度联盟、adHub，则pform=1000|1001|1003
目前广告平台对应id是：
1、广点通=1000
2、百度联盟=1001
3、有米=1002
4、adHub=1003
```

#### 2.选择合适的广告平台加载
        GET /v1/setting/adload
        
#### 接口说明
```
当广告isready还未好，开发者在调用preload、load函数时，会调用该接口，选择适合的平台广告进行加载
```
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appid           |string    | 广告平台分配给开发者的应用id            |是
pid             |string    | 我方广告平台的广告位id                 |是
pform            |string    | 开发者接入sdk，聚合了对应的广告平台数     |是

#### 返回结果说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
ad           |object    | 加载广告平台的广告位                  |是
adDefault             |object   | 当广告位加载失败或为空时，选择替代的广告配置|是


#### 返回示例
```json
{
	"code": 0,
	"msg": "",
	"data": {
		"ad": {
			"platformID": "默认加载广告平台ID",
			"platformName": "默认加载广告平台名称",
			"appdata": "$Base64(平台app维度基础配置json)",
			"ads": [{
					"adId": "当前广告平台下广告位",
					"adType": "广告类型"
				},
				{
					"adId": "当前广告平台下广告位",
					"adType": "广告类型"
				}
			]
		},
		"adDefault": {
			"platformID": "默认加载广告平台ID",
			"platformName": "默认加载广告平台名称",
			"appdata": "$Base64(平台app维度基础配置json)",
			"ads": [{
					"adId": "当前广告平台下广告位",
					"adType": "广告类型"
				},
				{
					"adId": "当前广告平台下广告位",
					"adType": "广告类型"
				}
			]
		}
	}
}
```




