##### 以下接口返回的结构体均一致，具体根据业务接口描述而定
##### 返回参数说明
参数字段 | 类型|说明
------------| -------------|-------------
code        |int    |0 表示处理成功，其他表示msg节点字段返回错误
msg	    |string |其它异常错误，正确则为空
objects|json object|是
obj|string|具体接口描述的对象|是

```json
{
    "code":0,
    "msg":"",
    "data":{
     "objects":[
      {
        "obj":"",
      }
     ]
    }
}
```

##### 1.广告应用平台下拉菜单列表加载
    GET /dev/common/appPlatformLoading

##### 2.应用分类下拉菜单列表加载
    GET /dev/common/appTypeLoading
    
##### 3.广告位类型下拉菜单列表加载
    GET /dev/common/adSpaceTypeLoading

##### 4.应用系统平台拉菜单列表加载
    GET /dev/common/appOsLoading




    
    

    



