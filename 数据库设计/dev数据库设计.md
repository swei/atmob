```mysql
create table app_ad_space
(
	id bigint auto_increment comment '应用广告位运营数据表流水ID'
		primary key,
	dev_id bigint null comment '开发者id',
	dev_name varchar(32) null comment '开发者账号',
	app_id varchar(32) null comment '应用atmob平台appid',
	app_name varchar(32) null comment '应用名称',
	app_platform int null comment '应用平台 1.安卓国内 2.google商店 3.ios应用商店 4.其他',
	app_platform_name varchar(32) null comment '应用平台',
	ad_space_name varchar(64) null comment '广告位名称',
	ad_space_status int null comment '广告位状态 1.正常 2.审核中 3.封禁；4.当前记录已删除；',
	ad_space_type int null comment '广告位类型 1.启屏 2.插屏 3.banner 4.激励视频 5.原生广告',
	atmob_pid varchar(32) null comment 'atmob平台广告位ID',
	app_create_time date null comment '应用创建时间',
	constraint app_ad_space_atmob_pid_uindex
		unique (atmob_pid)
)
comment '应用广告位管理信息表' engine=InnoDB
;

create index app_ad_space_app_name_index
	on app_ad_space (app_name)
;

create index app_ad_space_app_platform_name_index
	on app_ad_space (app_platform_name)
;

create index app_ad_space_ad_space_name_index
	on app_ad_space (ad_space_name)
;

create index app_ad_space_ad_space_type_index
	on app_ad_space (ad_space_type)
;

create table app_info
(
	id bigint auto_increment comment '应用管理ID'
		primary key,
	dev_id bigint null comment '开发者id',
	app_id varchar(32) null comment '开发者对应应用在atmob平台appID',
	app_os int null comment '应用类型',
	app_name varchar(32) null comment '应用名称',
	app_type int null comment '应用分类 1.游戏 2.。。。。。。',
	app_platform int null comment '应用平台 1.安卓国内 2.google商店 3.ios应用商店 4.其他',
	app_platform_name varchar(32) null comment '应用平台名称',
	app_packagename varchar(32) null comment '应用包名',
	app_status int null comment '当前应用状态 1.正常(运行中) 2.审核中 3.封禁 4.当前应用已经被删除',
	app_link varchar(1024) null comment '应用商店连接',
	app_create_time date null comment '当前应用创建时间',
	constraint app_info_app_id_uindex
		unique (app_id)
)
comment '应用管理信息表' engine=InnoDB
;

create index app_info_app_name_index
	on app_info (app_name)
;

create index app_info_app_platform_name_index
	on app_info (app_platform_name)
;

create table developer
(
	id bigint auto_increment comment '开发者ID'
		primary key,
	dev_name varchar(64) null comment '用户名称',
	password varchar(64) null comment '密码，md5串',
	email varchar(32) null comment '注册用户邮箱',
	company varchar(64) null comment '公司名称',
	create_time varchar(64) null comment '当前用户创建时间',
	user_status tinyint(1) null comment '启用：true  ，禁用 ：false',
	phone varchar(32) null comment '手机号码'
)
comment 'atmob聚合平台用户表' engine=InnoDB
;

create index user_email_index
	on developer (email)
;


```